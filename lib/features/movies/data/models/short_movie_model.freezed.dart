// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'short_movie_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

ShortMovieModel _$ShortMovieModelFromJson(Map<String, dynamic> json) {
  return _ShortMovieModel.fromJson(json);
}

/// @nodoc
mixin _$ShortMovieModel {
  int get id => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  String get shortDescription => throw _privateConstructorUsedError;
  RatingModel get rating => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ShortMovieModelCopyWith<ShortMovieModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ShortMovieModelCopyWith<$Res> {
  factory $ShortMovieModelCopyWith(
          ShortMovieModel value, $Res Function(ShortMovieModel) then) =
      _$ShortMovieModelCopyWithImpl<$Res, ShortMovieModel>;
  @useResult
  $Res call({int id, String name, String shortDescription, RatingModel rating});

  $RatingModelCopyWith<$Res> get rating;
}

/// @nodoc
class _$ShortMovieModelCopyWithImpl<$Res, $Val extends ShortMovieModel>
    implements $ShortMovieModelCopyWith<$Res> {
  _$ShortMovieModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? name = null,
    Object? shortDescription = null,
    Object? rating = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      shortDescription: null == shortDescription
          ? _value.shortDescription
          : shortDescription // ignore: cast_nullable_to_non_nullable
              as String,
      rating: null == rating
          ? _value.rating
          : rating // ignore: cast_nullable_to_non_nullable
              as RatingModel,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $RatingModelCopyWith<$Res> get rating {
    return $RatingModelCopyWith<$Res>(_value.rating, (value) {
      return _then(_value.copyWith(rating: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$ShortMovieModelImplCopyWith<$Res>
    implements $ShortMovieModelCopyWith<$Res> {
  factory _$$ShortMovieModelImplCopyWith(_$ShortMovieModelImpl value,
          $Res Function(_$ShortMovieModelImpl) then) =
      __$$ShortMovieModelImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int id, String name, String shortDescription, RatingModel rating});

  @override
  $RatingModelCopyWith<$Res> get rating;
}

/// @nodoc
class __$$ShortMovieModelImplCopyWithImpl<$Res>
    extends _$ShortMovieModelCopyWithImpl<$Res, _$ShortMovieModelImpl>
    implements _$$ShortMovieModelImplCopyWith<$Res> {
  __$$ShortMovieModelImplCopyWithImpl(
      _$ShortMovieModelImpl _value, $Res Function(_$ShortMovieModelImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? name = null,
    Object? shortDescription = null,
    Object? rating = null,
  }) {
    return _then(_$ShortMovieModelImpl(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      shortDescription: null == shortDescription
          ? _value.shortDescription
          : shortDescription // ignore: cast_nullable_to_non_nullable
              as String,
      rating: null == rating
          ? _value.rating
          : rating // ignore: cast_nullable_to_non_nullable
              as RatingModel,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$ShortMovieModelImpl implements _ShortMovieModel {
  _$ShortMovieModelImpl(
      {required this.id,
      required this.name,
      required this.shortDescription,
      required this.rating});

  factory _$ShortMovieModelImpl.fromJson(Map<String, dynamic> json) =>
      _$$ShortMovieModelImplFromJson(json);

  @override
  final int id;
  @override
  final String name;
  @override
  final String shortDescription;
  @override
  final RatingModel rating;

  @override
  String toString() {
    return 'ShortMovieModel(id: $id, name: $name, shortDescription: $shortDescription, rating: $rating)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ShortMovieModelImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.shortDescription, shortDescription) ||
                other.shortDescription == shortDescription) &&
            (identical(other.rating, rating) || other.rating == rating));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, id, name, shortDescription, rating);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ShortMovieModelImplCopyWith<_$ShortMovieModelImpl> get copyWith =>
      __$$ShortMovieModelImplCopyWithImpl<_$ShortMovieModelImpl>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$ShortMovieModelImplToJson(
      this,
    );
  }
}

abstract class _ShortMovieModel implements ShortMovieModel {
  factory _ShortMovieModel(
      {required final int id,
      required final String name,
      required final String shortDescription,
      required final RatingModel rating}) = _$ShortMovieModelImpl;

  factory _ShortMovieModel.fromJson(Map<String, dynamic> json) =
      _$ShortMovieModelImpl.fromJson;

  @override
  int get id;
  @override
  String get name;
  @override
  String get shortDescription;
  @override
  RatingModel get rating;
  @override
  @JsonKey(ignore: true)
  _$$ShortMovieModelImplCopyWith<_$ShortMovieModelImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
