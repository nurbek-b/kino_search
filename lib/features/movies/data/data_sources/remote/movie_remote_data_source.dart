import 'dart:developer';

import 'package:fox_video/core/constants/constants.dart';
import 'package:fox_video/core/error/error.dart';
import 'package:fox_video/core/network/network.dart';
import 'package:fox_video/core/use_case/base_use_case.dart';
import 'package:fox_video/features/movies/data/models/movie_model.dart';
import 'package:fox_video/features/movies/data/models/short_movie_model.dart';
import 'package:fox_video/features/movies/data/repositories/firestore_repository.dart';
import 'package:fox_video/features/movies/domain/entities/movie_entity.dart';
import 'package:fox_video/features/movies/domain/entities/short_movie_entity.dart';
import 'package:fox_video/features/movies/domain/use_cases/fetch_movie_use_case.dart';
import 'package:fox_video/features/movies/domain/use_cases/fetch_movies_use_case.dart';
import 'package:fox_video/features/movies/domain/use_cases/remove_movie_use_case.dart';
import 'package:fox_video/features/movies/domain/use_cases/save_movie_use_case.dart';
import 'package:fox_video/features/movies/domain/use_cases/search_movie_use_case.dart';

abstract class MovieRemoteDataSource {
  Future<void> saveMovie(SaveMoviesParams params);

  Future<void> deleteMovie(RemoveMoviesParams params);

  Future<List<MovieEntity>> fetchSelectedMovies(NoParams params);

  Future<MovieEntity> fetchMovie(FetchMovieParams params);

  Future<List<ShortMovieEntity>> fetchMovies(FetchMoviesParams params);

  Future<List<ShortMovieEntity>> searchProducts(SearchParams params);
}

class MovieRemoteDataSourceImpl implements MovieRemoteDataSource {
  final APIProvider apiProvider;
  final FireStoreRepository fireStoreRepository;

  MovieRemoteDataSourceImpl(
    this.apiProvider,
    this.fireStoreRepository,
  );

  @override
  Future<void> saveMovie(params) async {
    try {
      fireStoreRepository.saveMovie(params.movie);
    } catch (_) {
      FireStoreException();
    }
  }

  @override
  Future<void> deleteMovie(params) async {
    try {
      fireStoreRepository.removeMovie(params.movieId);
    } catch (_) {
      FireStoreException();
    }
  }

  @override
  Future<List<ShortMovieEntity>> fetchMovies(params) async => _getMoviesFromUrl(
        '$mainUrl?page_size=${params.limit}&page=${params.page}',
      );

  @override
  Future<List<ShortMovieEntity>> searchProducts(SearchParams params) async =>
      _getMoviesFromUrl(
        '$mainUrl?text=${params.query}&limit=${params.limit}&page=${params.page}',
      );

  /// generic function for getting list of [ProductModel]
  Future<List<ShortMovieEntity>> _getMoviesFromUrl(String url) async {
    log("==>> PRODUCT URL: $url");
    final response = await apiProvider.get(endPoint: url);
    log("RESPONSE: $response");
    log("FETCHING PRODUCT RESPONSE: len => ${response.data["results"].length}");
    return List<ShortMovieEntity>.from(
        response.data.map((item) => ShortMovieModel.fromJson(item)));
  }

  @override
  Future<MovieEntity> fetchMovie(FetchMovieParams params) async {
    final response =
        await apiProvider.get(endPoint: "$mainUrl/${params.movieId}");

    return MovieModel.fromJson(response.data);
  }

  @override
  Future<List<MovieEntity>> fetchSelectedMovies(NoParams params) async =>
      fireStoreRepository.fetchSelectedMovies();
}
