class ActorEntity {
  final int id;
  final String name;
  final String surname;
  final String image;

  const ActorEntity({
    required this.id,
    required this.name,
    required this.surname,
    required this.image,
  });
}
