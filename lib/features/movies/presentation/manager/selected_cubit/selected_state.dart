part of 'selected_cubit.dart';

sealed class SelectedState {}

class SelectedInitial extends SelectedState {}

class SelectedLoading extends SelectedState {}

class SelectedLoaded extends SelectedState {
  final List<MovieEntity> movies;

  SelectedLoaded(this.movies);
}

class SelectedError extends SelectedState {
  final ErrorObject errorObject;

  SelectedError({required this.errorObject});
}
