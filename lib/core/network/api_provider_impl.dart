import 'package:dio/dio.dart';
import 'package:fox_video/core/constants/constants.dart';
import 'package:fox_video/core/network/api_provider.dart';

class APIProviderImpl implements APIProvider {
  late Dio dio;

  APIProviderImpl() {
    dio = Dio()
      ..interceptors.addAll([
        // DioInterceptor(sl()),
        // if (kDebugMode)
        //   LogInterceptor(
        //     responseBody: true,
        //     requestBody: true,
        //   ),
      ])
      ..options.baseUrl = mainUrl;
  }

  @override
  Future<Response> get(
      {String? baseUrl,
      required String endPoint,
      data,
      query,
      CancelToken? cancelToken,
      int? timeOut,
      bool isMultipart = false}) async {
    if (timeOut != null) {
      dio.options.connectTimeout = Duration(milliseconds: timeOut);
    }

    dio.options.headers = {
      'x-api-key': kinoPoiskApiKey,
      if (isMultipart) 'Content-Type': 'multipart/form-data',
      if (!isMultipart) 'Content-Type': 'application/json',
      if (!isMultipart) 'Accept': 'application/json',
    };
    return await dio.get(
      endPoint,
      queryParameters: query,
      cancelToken: cancelToken,
    );
  }

  @override
  Future<Response> post(
      {String? baseUrl,
      required String endPoint,
      data,
      query,
      ProgressCallback? progressCallback,
      CancelToken? cancelToken,
      int? timeOut,
      bool isMultipart = false}) async {
    if (timeOut != null) {
      dio.options.connectTimeout = Duration(milliseconds: timeOut);
    }

    dio.options.headers = {
      'x-api-key': kinoPoiskApiKey,
      if (isMultipart) 'Content-Type': 'application/x-www-form-urlencoded',
      if (!isMultipart) 'Content-Type': 'application/json',
      if (!isMultipart) 'Accept': 'application/json',
    };

    return await dio.post(
      endPoint,
      data: data,
      queryParameters: query,
      onSendProgress: progressCallback,
      cancelToken: cancelToken,
    );
  }

  @override
  Future<Response> put(
      {String? baseUrl,
      required String endPoint,
      data,
      query,
      ProgressCallback? progressCallback,
      CancelToken? cancelToken,
      int? timeOut,
      bool isMultipart = false}) async {
    if (timeOut != null) {
      dio.options.connectTimeout = Duration(milliseconds: timeOut);
    }

    dio.options.headers = {
      'x-api-key': kinoPoiskApiKey,
      if (isMultipart) 'Content-Type': 'multipart/form-data',
      if (!isMultipart) 'Content-Type': 'application/json',
      if (!isMultipart) 'Accept': 'application/json',
    };
    return await dio.put(
      endPoint,
      data: data,
      queryParameters: query,
      onSendProgress: progressCallback,
      cancelToken: cancelToken,
    );
  }

  @override
  Future<Response> patch(
      {String? baseUrl,
      required String endPoint,
      data,
      query,
      ProgressCallback? progressCallback,
      CancelToken? cancelToken,
      int? timeOut,
      bool isMultipart = false}) async {
    if (timeOut != null) {
      dio.options.connectTimeout = Duration(milliseconds: timeOut);
    }

    dio.options.headers = {
      'x-api-key': kinoPoiskApiKey,
      if (isMultipart) 'Content-Type': 'multipart/form-data',
      if (!isMultipart) 'Content-Type': 'application/json',
      if (!isMultipart) 'Accept': 'application/json',
    };
    return await dio.patch(
      endPoint,
      data: data,
      queryParameters: query,
      onSendProgress: progressCallback,
      cancelToken: cancelToken,
    );
  }

  @override
  Future<Response> delete(
      {String? baseUrl,
      required String endPoint,
      data,
      query,
      CancelToken? cancelToken,
      ProgressCallback? progressCallback,
      int? timeOut,
      bool isMultipart = false}) async {
    if (timeOut != null) {
      dio.options.connectTimeout = Duration(milliseconds: timeOut);
    }

    dio.options.headers = {
      'x-api-key': kinoPoiskApiKey,
      if (isMultipart) 'Content-Type': 'multipart/form-data',
      if (!isMultipart) 'Content-Type': 'application/json',
      if (!isMultipart) 'Accept': 'application/json',
    };

    return await dio.delete(
      endPoint,
      data: data,
      cancelToken: cancelToken,
    );
  }
}
