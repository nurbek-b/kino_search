import 'package:fox_video/features/movies/domain/entities/actor_entity.dart';
import 'package:fox_video/features/movies/domain/entities/fee_entity.dart';
import 'package:fox_video/features/movies/domain/entities/genre_entity.dart';
import 'package:fox_video/features/movies/domain/entities/rating_entity.dart';
import 'package:fox_video/features/movies/domain/entities/vote_entity.dart';

class MovieEntity {
  final int id;
  final String name;
  final String description;
  final DateTime year;
  final RatingEntity rating;
  final VoteEntity votes;
  final List<Genre> genres;
  final DateTime? premiere;
  final String? teaser;
  final WorldFee? worldTotalFee;
  final List<ActorEntity> actors;

  const MovieEntity({
    required this.id,
    required this.name,
    required this.description,
    required this.year,
    required this.rating,
    required this.votes,
    required this.genres,
    required this.premiere,
    required this.teaser,
    required this.worldTotalFee,
    required this.actors,
  });
}
