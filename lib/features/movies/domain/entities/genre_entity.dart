class Genre {
  final String name;

  Genre(this.name);

  factory Genre.fromJson(Map<String, dynamic> json) => Genre(json["name"]);

  Map<String, dynamic> toJson() => {"name": name};
}
