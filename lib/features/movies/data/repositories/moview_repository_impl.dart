import 'dart:developer';

import 'package:dartz/dartz.dart';
import 'package:fox_video/core/error/error.dart';
import 'package:fox_video/core/network/network.dart';
import 'package:fox_video/core/repositories/data_repository.dart';
import 'package:fox_video/core/use_case/base_use_case.dart';
import 'package:fox_video/features/movies/data/data_sources/remote/movie_remote_data_source.dart';
import 'package:fox_video/features/movies/domain/entities/movie_entity.dart';
import 'package:fox_video/features/movies/domain/entities/short_movie_entity.dart';
import 'package:fox_video/features/movies/domain/repositories/movie_base_repository.dart';
import 'package:fox_video/features/movies/domain/use_cases/fetch_movie_use_case.dart';
import 'package:fox_video/features/movies/domain/use_cases/fetch_movies_use_case.dart';
import 'package:fox_video/features/movies/domain/use_cases/remove_movie_use_case.dart';
import 'package:fox_video/features/movies/domain/use_cases/save_movie_use_case.dart';
import 'package:fox_video/features/movies/domain/use_cases/search_movie_use_case.dart';

class MovieRepositoryImpl extends MovieBaseRepository {
  final MovieRemoteDataSource remoteDataSource;
  final DataRepository<MovieEntity> localDataSource;
  final NetworkInfo networkInfo;

  MovieRepositoryImpl(
    this.remoteDataSource,
    this.localDataSource,
    this.networkInfo,
  );

  @override
  Future<Either<FailureEntity, List<ShortMovieEntity>>> fetchMovies(
      FetchMoviesParams params) async {
    try {
      final data = await remoteDataSource.fetchMovies(params);
      return right(data);
    } on ServerException {
      return const Left(ServerFailure());
    } on DataParsingException {
      return const Left(DataParsingFailure());
    } on NoConnectionException {
      return const Left(NoConnectionFailure());
    } catch (err) {
      log("UNKNOWN: $err");
      return const Left(unKnown());
    }
  }

  @override
  Future<Either<FailureEntity, void>> removeMovie(
      RemoveMoviesParams params) async {
    if (await networkInfo.isConnected) {
      await remoteDataSource.deleteMovie(params);
      await localDataSource.deleteData(int.parse(params.movieId));
    } else {
      await localDataSource.deleteData(int.parse(params.movieId));
    }
    return const Right(null);
  }

  @override
  Future<Either<FailureEntity, void>> saveMovie(SaveMoviesParams params) async {
    if (await networkInfo.isConnected) {
      await remoteDataSource.saveMovie(params);
    } else {
      await localDataSource.writeData(params.movie);
    }
    return const Right(null);
  }

  @override
  Future<Either<FailureEntity, List<ShortMovieEntity>>> searchProducts(
      SearchParams params) async {
    try {
      final data = await remoteDataSource.searchProducts(params);
      return Right(data);
    } on ServerException {
      return const Left(ServerFailure());
    } on DataParsingException {
      return const Left(DataParsingFailure());
    } on NoConnectionException {
      return const Left(NoConnectionFailure());
    } catch (err) {
      log("UNKNOWN: $err");
      return const Left(unKnown());
    }
  }

  @override
  Future<Either<FailureEntity, MovieEntity>> fetchMovie(
      FetchMovieParams params) async {
    try {
      final data = await remoteDataSource.fetchMovie(params);
      return Right(data);
    } on ServerException {
      return const Left(ServerFailure());
    } on DataParsingException {
      return const Left(DataParsingFailure());
    } on NoConnectionException {
      return const Left(NoConnectionFailure());
    } catch (err) {
      log("UNKNOWN: $err");
      return const Left(unKnown());
    }
  }

  @override
  Future<Either<FailureEntity, List<MovieEntity>>> fetchSelectedMovies(
      NoParams params) async {
    try {
      final data = await remoteDataSource.fetchSelectedMovies(params);
      return Right(data);
    } on ServerException {
      return const Left(ServerFailure());
    } on DataParsingException {
      return const Left(DataParsingFailure());
    } on NoConnectionException {
      return const Left(NoConnectionFailure());
    } catch (err) {
      log("UNKNOWN: $err");
      return const Left(unKnown());
    }
  }
}
