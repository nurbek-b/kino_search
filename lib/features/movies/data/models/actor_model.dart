import 'package:fox_video/features/movies/domain/entities/actor_entity.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'actor_model.freezed.dart';

part 'actor_model.g.dart';

@freezed
class ActorModel extends ActorEntity with _$ActorModel {
  factory ActorModel({
    required int id,
    required String name,
    required String surname,
    required String image,
  }) = _ActorModel;

  factory ActorModel.fromJson(Map<String, dynamic> json) =>
      _$ActorModelFromJson(json);

  factory ActorModel.fromEntity(ActorEntity entity) =>
      ActorModel(
        id: entity.id,
        name: entity.name,
        surname: entity.surname,
        image: entity.image,
      );
}
