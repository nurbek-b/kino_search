abstract class DataRepository<T> {
  const DataRepository();

  Future<List<T>> readAll();

  Future<void> clearAll();

  Future<void> writeData(T model);

  Future<void> deleteData(int id);
}
