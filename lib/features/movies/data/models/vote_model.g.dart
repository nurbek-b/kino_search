// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'vote_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$VoteModelImpl _$$VoteModelImplFromJson(Map<String, dynamic> json) =>
    _$VoteModelImpl(
      kp: json['kp'] as int,
      imdb: json['imdb'] as int,
      filmCritics: json['filmCritics'] as int,
      russianFilmCritics: json['russianFilmCritics'] as int,
      tmdb: json['tmdb'] as int? ?? 0,
    );

Map<String, dynamic> _$$VoteModelImplToJson(_$VoteModelImpl instance) =>
    <String, dynamic>{
      'kp': instance.kp,
      'imdb': instance.imdb,
      'filmCritics': instance.filmCritics,
      'russianFilmCritics': instance.russianFilmCritics,
      'tmdb': instance.tmdb,
    };
