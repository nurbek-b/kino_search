import 'dart:convert';

import 'package:fox_video/features/movies/data/models/actor_model.dart';
import 'package:fox_video/features/movies/data/models/movie_model.dart';
import 'package:path/path.dart';

// ignore: depend_on_referenced_packages
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class SQLInterface<T> {
  SQLInterface({required this.tableName});

  static Database? _db;

  static const movieDataTable = 'movie_data_table';
  static const actorDataTable = 'actor_data_table';
  static const movieActorDataTable = 'movie_actors_data_table';
  static const keyId = 'id';
  static const movieKeyId = 'movie_id';
  static const actorKeyId = 'actor_id';
  static const keyDate = 'date_utc';
  static const keyBody = 'json_body';
  static const version = 1;

  final String tableName;

  static Future<Database> _initDB() async {
    final databasesPath = await getApplicationDocumentsDirectory();
    final path = join(databasesPath.path, 'fox.db');

    final db = await openDatabase(
      path,
      version: version,
      onCreate: (db, version) async {
        await db.execute(
          'CREATE TABLE movies('
          'id INTEGER PRIMARY KEY, '
          'title TEXT, '
          'description TEXT, '
          'released TEXT, '
          'vote_amount INTEGER, '
          'age_rating INTEGER, '
          'genre TEXT, '
          'world_total_fee REAL, '
          'world_premier_date TEXT, '
          'teaser TEXT)',
        );
        await db.execute(
          'CREATE TABLE actors('
          'id INTEGER PRIMARY KEY, '
          'name TEXT, '
          'surname TEXT, '
          'image TEXT)',
        );
        await db.execute(
          'CREATE TABLE movie_actors('
          'movie_id INTEGER, '
          'actor_id INTEGER, '
          'FOREIGN KEY(movie_id) REFERENCES movies(id), '
          'FOREIGN KEY(actor_id) REFERENCES actors(id), '
          'PRIMARY KEY(movie_id, actor_id))',
        );
      },
    );

    return db;
  }

  Future<Database> get db async {
    if (_db != null) return _db!;

    return _db = await _initDB();
  }

  Future<void> insertItem({
    required int itemId,
    required MovieModel movie,
  }) async =>
      (await (await db).transaction((txn) async {
        final movieId = await txn.insert(
          movieDataTable,
          movie.toJson(),
          conflictAlgorithm: ConflictAlgorithm.replace,
        );

        for (final actor in movie.actors) {
          final actorId = await txn.insert(
            actorDataTable,
            ActorModel.fromEntity(actor).toJson(),
            conflictAlgorithm: ConflictAlgorithm.replace,
          );

          await txn.insert(
            movieActorDataTable,
            {movieKeyId: movieId, actorKeyId: actorId},
            conflictAlgorithm: ConflictAlgorithm.replace,
          );
        }
      }));

  Future<List<T>> getAllItems(TypeConverter<T> converter) async {
    final items = await (await db).query(
      tableName,
      orderBy: keyId,
    );

    return items
        .map(
          (item) => converter(jsonDecode(
            item[keyBody] as String,
          ) as Map<String, dynamic>),
        )
        .toList();
  }

  Future<bool> deleteById(List<int> valueIds) async =>
      (await (await db).delete(
        movieDataTable,
        where: 'id = ?',
        whereArgs: valueIds,
      )) >
      0;

  Future<void> clearAllData() async => (await db).delete(tableName);

  Future close() async => (await db).close();
}

typedef TypeConverter<T> = T Function(Map<String, dynamic>);
