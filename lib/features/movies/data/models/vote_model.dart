import 'package:fox_video/features/movies/domain/entities/vote_entity.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'vote_model.freezed.dart';

part 'vote_model.g.dart';

@freezed
class VoteModel extends VoteEntity with _$VoteModel {
  factory VoteModel({
    required int kp,
    required int imdb,
    required int filmCritics,
    required int russianFilmCritics,
    @Default(0) int tmdb,
  }) = _VoteModel;

  factory VoteModel.fromJson(Map<String, dynamic> json) =>
      _$VoteModelFromJson(json);

  factory VoteModel.fromEntity(VoteEntity entity) => VoteModel(
        kp: entity.kp,
        imdb: entity.imdb,
        filmCritics: entity.filmCritics,
        russianFilmCritics: entity.russianFilmCritics,
      );
}
