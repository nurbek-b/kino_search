import 'package:flutter/material.dart';

class AppPageRoute extends PageRouteBuilder {
  static const _duration = Duration(milliseconds: 250);

  AppPageRoute({required Widget screen, RouteSettings? routeSetting})
      : super(
          settings: routeSetting,
          transitionDuration: _duration,
          reverseTransitionDuration: _duration,
          transitionsBuilder: (_, animation, secondAnimation, child) {
            final tweenAnimation = Tween<Offset>(
              begin: const Offset(1, 0),
              end: Offset.zero,
            ).animate(
                CurvedAnimation(parent: animation, curve: Curves.decelerate));

            return SlideTransition(
              position: tweenAnimation,
              child: child,
            );
          },
          pageBuilder: (_, animation, secondAnimation) => screen,
        );
}
