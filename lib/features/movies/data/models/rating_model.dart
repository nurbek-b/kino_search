import 'package:fox_video/features/movies/domain/entities/rating_entity.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'rating_model.freezed.dart';

part 'rating_model.g.dart';

@freezed
class RatingModel extends RatingEntity with _$RatingModel {
  factory RatingModel({
    required int kp,
    required int imdb,
    required int filmCritics,
    required int russianFilmCritics,
    @Default(0) int tmdb,
  }) = _RatingModel;

  factory RatingModel.fromJson(Map<String, dynamic> json) =>
      _$RatingModelFromJson(json);

  factory RatingModel.fromEntity(RatingEntity entity) => RatingModel(
      kp: entity.kp,
      imdb: entity.imdb,
      filmCritics: entity.filmCritics,
      russianFilmCritics: entity.russianFilmCritics);
}
