import 'package:fox_video/core/extensions/sql.dart';
import 'package:fox_video/core/repositories/data_repository.dart';
import 'package:fox_video/features/movies/data/models/movie_model.dart';
import 'package:fox_video/features/movies/domain/entities/movie_entity.dart';

class MovieLocalDataSource implements DataRepository<MovieEntity> {
  const MovieLocalDataSource({
    required this.dataBase,
    required this.dataConverter,
  });

  final SQLInterface<MovieModel> dataBase;
  final TypeConverter<MovieModel> dataConverter;

  @override
  Future<List<MovieEntity>> readAll() async =>
      dataBase.getAllItems(dataConverter);

  @override
  Future<void> writeData(MovieEntity model) async => dataBase.insertItem(
      itemId: model.id, movie: MovieModel.fromEntity(model));

  @override
  Future<void> deleteData(int id) => dataBase.deleteById([id]);

  @override
  Future<void> clearAll() async => dataBase.clearAllData();

  void close() => dataBase.close();
}
