// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'short_movie_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$ShortMovieModelImpl _$$ShortMovieModelImplFromJson(
        Map<String, dynamic> json) =>
    _$ShortMovieModelImpl(
      id: json['id'] as int,
      name: json['name'] as String,
      shortDescription: json['shortDescription'] as String,
      rating: RatingModel.fromJson(json['rating'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$ShortMovieModelImplToJson(
        _$ShortMovieModelImpl instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'shortDescription': instance.shortDescription,
      'rating': instance.rating,
    };
