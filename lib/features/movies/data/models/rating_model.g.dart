// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'rating_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$RatingModelImpl _$$RatingModelImplFromJson(Map<String, dynamic> json) =>
    _$RatingModelImpl(
      kp: json['kp'] as int,
      imdb: json['imdb'] as int,
      filmCritics: json['filmCritics'] as int,
      russianFilmCritics: json['russianFilmCritics'] as int,
      tmdb: json['tmdb'] as int? ?? 0,
    );

Map<String, dynamic> _$$RatingModelImplToJson(_$RatingModelImpl instance) =>
    <String, dynamic>{
      'kp': instance.kp,
      'imdb': instance.imdb,
      'filmCritics': instance.filmCritics,
      'russianFilmCritics': instance.russianFilmCritics,
      'tmdb': instance.tmdb,
    };
