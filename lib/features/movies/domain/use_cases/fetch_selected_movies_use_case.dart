import 'package:dartz/dartz.dart';
import 'package:fox_video/core/error/error.dart';
import 'package:fox_video/core/use_case/base_use_case.dart';
import 'package:fox_video/features/movies/domain/entities/movie_entity.dart';
import 'package:fox_video/features/movies/domain/repositories/movie_base_repository.dart';

class FetchSelectedMoviesUseCase
    extends BaseUseCase<List<MovieEntity>, NoParams> {
  final MovieBaseRepository _repository;

  FetchSelectedMoviesUseCase(this._repository);

  @override
  Future<Either<FailureEntity, List<MovieEntity>>> call(NoParams params) async {
    return await _repository.fetchSelectedMovies(params);
  }
}
