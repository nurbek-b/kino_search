import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fox_video/app/app.dart';
import 'package:fox_video/core/theme/app_theme.dart';
import 'package:fox_video/features/movies/presentation/manager/manager.dart';
import 'package:fox_video/firebase_options.dart';
import 'package:fox_video/injection_container.dart';
import 'package:provider/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);
  await AppDependencies().initialize();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (BuildContext context) => AppTheme(),
      child: MultiBlocProvider(
        providers: [
          BlocProvider(create: (context) => MovieBloc(sl(), sl())),
          BlocProvider(create: (context) => SearchBloc(sl())),
          BlocProvider(create: (context) => SelectedCubit(sl(), sl(), sl())),
        ],
        child: Consumer<AppTheme>(
          builder: (context, appTheme, _) {
            return MaterialApp(
              title: 'FoxVideo',
              theme: appTheme.themeData,
              debugShowCheckedModeBanner: false,
              home: const HomePage(),
            );
          },
        ),
      ),
    );
  }
}
