import 'package:fox_video/features/movies/domain/entities/rating_entity.dart';

class ShortMovieEntity {
  final int id;
  final String name;
  final String shortDescription;
  final RatingEntity rating;

  const ShortMovieEntity({
    required this.id,
    required this.name,
    required this.shortDescription,
    required this.rating,
  });
}
