// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'vote_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

VoteModel _$VoteModelFromJson(Map<String, dynamic> json) {
  return _VoteModel.fromJson(json);
}

/// @nodoc
mixin _$VoteModel {
  int get kp => throw _privateConstructorUsedError;
  int get imdb => throw _privateConstructorUsedError;
  int get filmCritics => throw _privateConstructorUsedError;
  int get russianFilmCritics => throw _privateConstructorUsedError;
  int get tmdb => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $VoteModelCopyWith<VoteModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $VoteModelCopyWith<$Res> {
  factory $VoteModelCopyWith(VoteModel value, $Res Function(VoteModel) then) =
      _$VoteModelCopyWithImpl<$Res, VoteModel>;
  @useResult
  $Res call(
      {int kp, int imdb, int filmCritics, int russianFilmCritics, int tmdb});
}

/// @nodoc
class _$VoteModelCopyWithImpl<$Res, $Val extends VoteModel>
    implements $VoteModelCopyWith<$Res> {
  _$VoteModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? kp = null,
    Object? imdb = null,
    Object? filmCritics = null,
    Object? russianFilmCritics = null,
    Object? tmdb = null,
  }) {
    return _then(_value.copyWith(
      kp: null == kp
          ? _value.kp
          : kp // ignore: cast_nullable_to_non_nullable
              as int,
      imdb: null == imdb
          ? _value.imdb
          : imdb // ignore: cast_nullable_to_non_nullable
              as int,
      filmCritics: null == filmCritics
          ? _value.filmCritics
          : filmCritics // ignore: cast_nullable_to_non_nullable
              as int,
      russianFilmCritics: null == russianFilmCritics
          ? _value.russianFilmCritics
          : russianFilmCritics // ignore: cast_nullable_to_non_nullable
              as int,
      tmdb: null == tmdb
          ? _value.tmdb
          : tmdb // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$VoteModelImplCopyWith<$Res>
    implements $VoteModelCopyWith<$Res> {
  factory _$$VoteModelImplCopyWith(
          _$VoteModelImpl value, $Res Function(_$VoteModelImpl) then) =
      __$$VoteModelImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int kp, int imdb, int filmCritics, int russianFilmCritics, int tmdb});
}

/// @nodoc
class __$$VoteModelImplCopyWithImpl<$Res>
    extends _$VoteModelCopyWithImpl<$Res, _$VoteModelImpl>
    implements _$$VoteModelImplCopyWith<$Res> {
  __$$VoteModelImplCopyWithImpl(
      _$VoteModelImpl _value, $Res Function(_$VoteModelImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? kp = null,
    Object? imdb = null,
    Object? filmCritics = null,
    Object? russianFilmCritics = null,
    Object? tmdb = null,
  }) {
    return _then(_$VoteModelImpl(
      kp: null == kp
          ? _value.kp
          : kp // ignore: cast_nullable_to_non_nullable
              as int,
      imdb: null == imdb
          ? _value.imdb
          : imdb // ignore: cast_nullable_to_non_nullable
              as int,
      filmCritics: null == filmCritics
          ? _value.filmCritics
          : filmCritics // ignore: cast_nullable_to_non_nullable
              as int,
      russianFilmCritics: null == russianFilmCritics
          ? _value.russianFilmCritics
          : russianFilmCritics // ignore: cast_nullable_to_non_nullable
              as int,
      tmdb: null == tmdb
          ? _value.tmdb
          : tmdb // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$VoteModelImpl implements _VoteModel {
  _$VoteModelImpl(
      {required this.kp,
      required this.imdb,
      required this.filmCritics,
      required this.russianFilmCritics,
      this.tmdb = 0});

  factory _$VoteModelImpl.fromJson(Map<String, dynamic> json) =>
      _$$VoteModelImplFromJson(json);

  @override
  final int kp;
  @override
  final int imdb;
  @override
  final int filmCritics;
  @override
  final int russianFilmCritics;
  @override
  @JsonKey()
  final int tmdb;

  @override
  String toString() {
    return 'VoteModel(kp: $kp, imdb: $imdb, filmCritics: $filmCritics, russianFilmCritics: $russianFilmCritics, tmdb: $tmdb)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$VoteModelImpl &&
            (identical(other.kp, kp) || other.kp == kp) &&
            (identical(other.imdb, imdb) || other.imdb == imdb) &&
            (identical(other.filmCritics, filmCritics) ||
                other.filmCritics == filmCritics) &&
            (identical(other.russianFilmCritics, russianFilmCritics) ||
                other.russianFilmCritics == russianFilmCritics) &&
            (identical(other.tmdb, tmdb) || other.tmdb == tmdb));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, kp, imdb, filmCritics, russianFilmCritics, tmdb);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$VoteModelImplCopyWith<_$VoteModelImpl> get copyWith =>
      __$$VoteModelImplCopyWithImpl<_$VoteModelImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$VoteModelImplToJson(
      this,
    );
  }
}

abstract class _VoteModel implements VoteModel {
  factory _VoteModel(
      {required final int kp,
      required final int imdb,
      required final int filmCritics,
      required final int russianFilmCritics,
      final int tmdb}) = _$VoteModelImpl;

  factory _VoteModel.fromJson(Map<String, dynamic> json) =
      _$VoteModelImpl.fromJson;

  @override
  int get kp;
  @override
  int get imdb;
  @override
  int get filmCritics;
  @override
  int get russianFilmCritics;
  @override
  int get tmdb;
  @override
  @JsonKey(ignore: true)
  _$$VoteModelImplCopyWith<_$VoteModelImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
