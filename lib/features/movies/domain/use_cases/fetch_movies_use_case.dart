import 'package:dartz/dartz.dart';
import 'package:fox_video/core/error/error.dart';
import 'package:fox_video/core/use_case/base_use_case.dart';
import 'package:fox_video/features/movies/domain/entities/short_movie_entity.dart';
import 'package:fox_video/features/movies/domain/repositories/movie_base_repository.dart';

class FetchMoviesUseCase
    extends BaseUseCase<List<ShortMovieEntity>, FetchMoviesParams> {
  final MovieBaseRepository _repository;

  FetchMoviesUseCase(this._repository);

  @override
  Future<Either<FailureEntity, List<ShortMovieEntity>>> call(
      FetchMoviesParams params) async {
    return await _repository.fetchMovies(params);
  }
}

class FetchMoviesParams {
  final int page;
  final int limit;

  FetchMoviesParams({
    this.page = 1,
    this.limit = 30,
  });

  Map<String, dynamic> toJson() {
    return {
      "page": page,
      "limit": limit,
    };
  }
}
