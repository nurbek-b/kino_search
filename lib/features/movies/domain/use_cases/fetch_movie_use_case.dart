import 'package:dartz/dartz.dart';
import 'package:fox_video/core/error/error.dart';
import 'package:fox_video/core/use_case/base_use_case.dart';
import 'package:fox_video/features/movies/domain/entities/movie_entity.dart';
import 'package:fox_video/features/movies/domain/repositories/movie_base_repository.dart';

class FetchMovieUseCase extends BaseUseCase<MovieEntity, FetchMovieParams> {
  final MovieBaseRepository _repository;

  FetchMovieUseCase(this._repository);

  @override
  Future<Either<FailureEntity, MovieEntity>> call(
      FetchMovieParams params) async {
    return await _repository.fetchMovie(params);
  }
}

class FetchMovieParams {
  final int movieId;

  FetchMovieParams({required this.movieId});
}
