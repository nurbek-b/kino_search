class WorldFee {
  final int? fee;
  final String? currency;

  const WorldFee({
    this.fee,
    this.currency,
  });

  factory WorldFee.fromJson(Map<String, dynamic> json) {
    return WorldFee(
      fee: json["fee"],
      currency: json["currency"],
    );
  }
}
