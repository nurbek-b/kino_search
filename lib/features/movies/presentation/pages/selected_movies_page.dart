import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fox_video/core/extensions/context_extension.dart';
import 'package:fox_video/features/movies/presentation/manager/manager.dart';
import 'package:fox_video/features/movies/presentation/widgets/error_view.dart';
import 'package:fox_video/features/movies/presentation/widgets/loader_view.dart';

class SelectedMoviesPage extends StatefulWidget {
  static const routeName = '/favourite_movies_page';

  const SelectedMoviesPage({super.key});

  @override
  State<SelectedMoviesPage> createState() => _SelectedMoviesPageState();
}

class _SelectedMoviesPageState extends State<SelectedMoviesPage> {
  @override
  void initState() {
    super.initState();
    context.read<SelectedCubit>().fetchSelectedMovies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: context.pop,
          icon: const Icon(Icons.chevron_left),
        ),
      ),
      body: BlocBuilder<SelectedCubit, SelectedState>(
        builder: (context, state) {
          if (state is SelectedLoading) {
            return const Loader();
          }
          if (state is SelectedError) {
            return ErrorView(
              title: state.errorObject.title,
              message: state.errorObject.message,
            );
          }
          if (state is SelectedLoaded) {
            if (state.movies.isEmpty) {
              return const Center(
                child: Text("No selected movies, yet!"),
              );
            }
            return ListView.builder(
              itemCount: state.movies.length,
              itemBuilder: (context, index) => ListTile(
                title: Text(state.movies[index].name),
                subtitle:
                    Text(state.movies[index].description.substring(0, 50)),
              ),
            );
          }
          return const SizedBox.shrink();
        },
      ),
    );
  }
}
