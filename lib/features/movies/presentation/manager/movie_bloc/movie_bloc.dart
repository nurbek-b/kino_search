import 'dart:developer';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fox_video/core/error/error.dart';
import 'package:fox_video/features/movies/domain/entities/movie_entity.dart';
import 'package:fox_video/features/movies/domain/entities/short_movie_entity.dart';
import 'package:fox_video/features/movies/domain/use_cases/fetch_movie_use_case.dart';
import 'package:fox_video/features/movies/domain/use_cases/fetch_movies_use_case.dart';

part 'movie_event.dart';

part 'movie_state.dart';

class MovieBloc extends Bloc<MovieEvent, MovieState> {
  final FetchMoviesUseCase fetchMoviesUseCase;
  final FetchMovieUseCase fetchMovieUseCase;

  MovieBloc(
    this.fetchMoviesUseCase,
    this.fetchMovieUseCase,
  ) : super(MovieInitial()) {
    on<FetchMovies>(_fetchMovies);
    on<FetchMovie>(_fetchMovie);
    fetchMovies();
  }

  void fetchMovies() => add(FetchMovies());

  Future<void> _fetchMovies(FetchMovies event, Emitter<MovieState> emit) async {
    if (state is LoadedState) {
      final currentState = state as LoadedState;
      if (!currentState.hasMore) return;
      emit(currentState.copyWith(loading: true));
      final failureOrSuccess = await fetchMoviesUseCase(
          FetchMoviesParams(page: currentState.page + 1));
      failureOrSuccess.fold(
        (failure) => emit(FailureState(
            errorObject:
                ErrorObject.mapFailureToErrorObject(failure: failure))),
        (success) => emit(
          currentState.copyWith(
            movies: List.of(currentState.movies)..addAll(success),
            hasMore: success.length < 30 ? false : true,
            page: currentState.page + 1,
            loading: false,
          ),
        ),
      );
    } else {
      emit(LoadingState());
      final failureOrSuccess =
          await fetchMoviesUseCase(FetchMoviesParams(page: 1));
      log("failureOrSuccess: $failureOrSuccess");
      failureOrSuccess.fold(
        (failure) => emit(FailureState(
            errorObject:
                ErrorObject.mapFailureToErrorObject(failure: failure))),
        (success) => emit(LoadedState(
          movies: success,
          hasMore: success.length < 30 ? false : true,
          page: 2,
        )),
      );
    }
  }

  void fetchMovie(int movieId) => add(FetchMovie(movieId));

  Future<void> _fetchMovie(FetchMovie event, Emitter<MovieState> emit) async {
    emit(LoadingState());
    final failureOrSuccess =
        await fetchMovieUseCase(FetchMovieParams(movieId: event.movieId));

    failureOrSuccess.fold(
      (failure) => emit(FailureState(
          errorObject: ErrorObject.mapFailureToErrorObject(failure: failure))),
      (success) => emit(
        MovieDetailLoaded(success),
      ),
    );
  }
}
