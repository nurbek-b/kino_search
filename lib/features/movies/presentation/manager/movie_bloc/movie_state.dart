part of 'movie_bloc.dart';

sealed class MovieState {}

class MovieInitial extends MovieState {}

class LoadingState extends MovieState {}

class FailureState extends MovieState {
  final ErrorObject errorObject;

  FailureState({required this.errorObject});
}

class LoadedState extends MovieState {
  final List<ShortMovieEntity> movies;
  final int page;
  final bool hasMore;
  final bool loading;

  LoadedState({
    this.movies = const [],
    this.page = 1,
    this.hasMore = false,
    this.loading = false,
  });

  LoadedState copyWith({
    List<ShortMovieEntity>? movies,
    int? page,
    bool? hasMore,
    bool? loading,
  }) {
    return LoadedState(
      movies: movies ?? this.movies,
      page: page ?? this.page,
      hasMore: hasMore ?? this.hasMore,
      loading: loading ?? this.loading,
    );
  }
}

class MovieDetailLoaded extends MovieState {
  final MovieEntity movie;

  MovieDetailLoaded(this.movie);
}
