import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fox_video/core/error/error.dart';
import 'package:fox_video/features/movies/domain/entities/short_movie_entity.dart';
import 'package:fox_video/features/movies/domain/use_cases/search_movie_use_case.dart';
import 'package:rxdart/transformers.dart';

part 'search_event.dart';

part 'search_state.dart';

const _duration = Duration(milliseconds: 300);

EventTransformer<TextChanged> debounceTransformer<TextChanged>(
    Duration duration) {
  return (events, mapper) {
    return events.debounceTime(duration).switchMap(mapper);
  };
}

class SearchBloc extends Bloc<SearchEvent, SearchState> {
  final SearchMoviesUseCase searchProductUseCase;

  SearchBloc(this.searchProductUseCase) : super(SearchInitial()) {
    on<TextChanged>(_onTextChanged,
        transformer: debounceTransformer(_duration));
  }

  void _onTextChanged(
    TextChanged event,
    Emitter<SearchState> emit,
  ) async {
    emit(SearchStateLoading());

    if (event.query.isEmpty) {
      return emit(SearchStateEmpty());
    }

    final failureOrSuccess = await searchProductUseCase.call(SearchParams(
      query: event.query,
      limit: 30,
    ));
    emit(
      failureOrSuccess.fold(
        (error) => SearchStateError(
            errorObject: ErrorObject.mapFailureToErrorObject(failure: error)),
        (items) => items.isEmpty
            ? SearchStateEmpty()
            : SearchStateSuccess(
                movies: items,
                query: event.query,
              ),
      ),
    );
  }
}
