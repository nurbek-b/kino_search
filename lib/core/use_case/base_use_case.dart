import 'package:dartz/dartz.dart';
import 'package:fox_video/core/error/error.dart';

abstract class BaseUseCase<T, Params> {
  Future<Either<FailureEntity, T>> call(Params params);
}

class NoParams {}
