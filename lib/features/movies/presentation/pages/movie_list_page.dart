import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fox_video/features/movies/presentation/manager/movie_bloc/movie_bloc.dart';
import 'package:fox_video/features/movies/presentation/widgets/error_view.dart';
import 'package:fox_video/features/movies/presentation/widgets/loader_view.dart';

import 'package:fox_video/features/movies/presentation/widgets/movie_list_tile.dart';

class MovieListPage extends StatelessWidget {
  static const routeName = '/movie_list';

  const MovieListPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MovieBloc, MovieState>(
      builder: (context, state) {
        log("MOVIE STATE: $state");
        if (state is LoadingState) {
          return const Loader();
        }
        if (state is FailureState) {
          return ErrorView(
            title: state.errorObject.title,
            message: state.errorObject.message,
          );
        }
        if (state is LoadedState) {
          return ListView.builder(
            itemCount: state.movies.length,
            itemBuilder: (context, index) => CustomListTile(
              movie: state.movies[index],
            ),
          );
        }
        return const SizedBox.shrink();
      },
    );
  }
}
