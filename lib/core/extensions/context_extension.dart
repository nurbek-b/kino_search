import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fox_video/core/theme/app_theme.dart';

extension ContextExtensions on BuildContext {
  /// Sizes
  MediaQueryData get mq => MediaQuery.of(this);

  double get screenWidth => mq.size.width;

  double get screenHeight => mq.size.height;

  double get topPadding => mq.padding.top;

  /// Theme
  AppTheme get appTheme => read<AppTheme>();

  /// Navigation
  void pushToRoot() => Navigator.of(this).popUntil((route) => route.isFirst);

  void pop<T>([T? result]) => Navigator.of(this).pop<T?>(result);

  Future<T?> pushNamed<T>(String routeName, {Object? arguments}) =>
      Navigator.of(this).pushNamed<T?>(routeName, arguments: arguments);

  Future pushAndReplaceNamed(String routeName, {Object? arguments}) async {
    pushToRoot();
    await Navigator.of(this)
        .pushReplacementNamed(routeName, arguments: arguments);
  }

  /// Deprecated, use [furnitureTheme.screenScaleFactor] instead
  double getProportionateScreenHeight(double inputHeight) {
    final screenHeight = this.screenHeight;
    return (inputHeight / AppTheme.defaultScreenHeight) * screenHeight;
  }
}
