import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fox_video/core/extensions/context_extension.dart';
import 'package:fox_video/core/theme/app_colors.dart';
import 'package:fox_video/features/movies/presentation/manager/manager.dart';
import 'package:fox_video/features/movies/presentation/pages/selected_movies_page.dart';
import 'package:fox_video/features/movies/presentation/pages/movie_list_page.dart';
import 'package:fox_video/features/movies/presentation/widgets/movie_list_tile.dart';
import 'package:fox_video/features/movies/presentation/widgets/search_field.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.primary,
        actions: [
          const SearchWidget(),
          IconButton(
            onPressed: () {
              context.pushNamed(SelectedMoviesPage.routeName);
            },
            icon: const Icon(
              Icons.favorite,
              color: AppColors.error,
            ),
          ),
        ],
      ),
      body: BlocBuilder<SearchBloc, SearchState>(
        builder: (context, state) {
          if (state is SearchStateSuccess) {
            return ListView.builder(
              itemCount: state.movies.length,
              itemBuilder: (context, index) => CustomListTile(
                movie: state.movies[index],
              ),
            );
          } else {
            return const MovieListPage();
          }
        },
      ),
    );
  }
}
