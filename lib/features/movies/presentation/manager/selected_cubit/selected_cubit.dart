import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fox_video/core/error/error.dart';
import 'package:fox_video/core/use_case/base_use_case.dart';
import 'package:fox_video/features/movies/domain/entities/movie_entity.dart';
import 'package:fox_video/features/movies/domain/use_cases/fetch_selected_movies_use_case.dart';
import 'package:fox_video/features/movies/domain/use_cases/remove_movie_use_case.dart';
import 'package:fox_video/features/movies/domain/use_cases/save_movie_use_case.dart';

part 'selected_state.dart';

class SelectedCubit extends Cubit<SelectedState> {
  final RemoveMovieUseCase removeMovieUseCase;
  final SaveMovieUseCase saveMovieUseCase;
  final FetchSelectedMoviesUseCase selectedMoviesUseCase;

  SelectedCubit(
    this.removeMovieUseCase,
    this.saveMovieUseCase,
    this.selectedMoviesUseCase,
  ) : super(SelectedInitial());

  Future<void> fetchSelectedMovies() async {
    emit(SelectedLoading());
    final failureOrSuccess = await selectedMoviesUseCase(NoParams());
    failureOrSuccess.fold(
      (error) => SelectedError(
          errorObject: ErrorObject.mapFailureToErrorObject(failure: error)),
      (success) => SelectedLoaded(success),
    );
  }

  Future<void> saveMovie(MovieEntity movie) async {
    saveMovieUseCase(SaveMoviesParams(movie));
  }

  Future<void> removeMovie(int movieId) async {
    removeMovieUseCase(RemoveMoviesParams(movieId.toString()));
  }
}
