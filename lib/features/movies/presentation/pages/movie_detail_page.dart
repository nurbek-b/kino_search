import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fox_video/core/extensions/context_extension.dart';
import 'package:fox_video/features/movies/presentation/manager/manager.dart';
import 'package:fox_video/features/movies/presentation/widgets/error_view.dart';
import 'package:fox_video/features/movies/presentation/widgets/loader_view.dart';

class MovieDetail extends StatefulWidget {
  static const routeName = '/movie_detail';
  final int movieId;

  const MovieDetail({
    super.key,
    required this.movieId,
  });

  static Route route({required int movieId}) {
    return MaterialPageRoute(
      settings: const RouteSettings(name: routeName),
      builder: (context) => MovieDetail(movieId: movieId),
    );
  }

  @override
  State<MovieDetail> createState() => _MovieDetailState();
}

class _MovieDetailState extends State<MovieDetail> {
  @override
  void initState() {
    super.initState();
    context.read<MovieBloc>().fetchMovie(widget.movieId);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            context.read<MovieBloc>().fetchMovies();
            context.pop();
          },
          icon: const Icon(Icons.chevron_left),
        ),
      ),
      body: BlocBuilder<MovieBloc, MovieState>(
        builder: (context, state) {
          if (state is LoadingState) {
            return const Loader();
          }
          if (state is FailureState) {
            return ErrorView(
              title: state.errorObject.title,
              message: state.errorObject.message,
            );
          }
          if (state is MovieDetailLoaded) {
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text("Movie detail loaded"),
                Text(state.movie.name)
              ],
            );
          }
          return const SizedBox.shrink();
        },
      ),
    );
  }
}
