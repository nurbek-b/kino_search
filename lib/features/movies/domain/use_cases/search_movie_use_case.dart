import 'package:dartz/dartz.dart';
import 'package:fox_video/core/error/error.dart';
import 'package:fox_video/core/use_case/base_use_case.dart';
import 'package:fox_video/features/movies/domain/entities/short_movie_entity.dart';
import 'package:fox_video/features/movies/domain/repositories/movie_base_repository.dart';

class SearchMoviesUseCase
    extends BaseUseCase<List<ShortMovieEntity>, SearchParams> {
  final MovieBaseRepository productRepository;

  SearchMoviesUseCase(this.productRepository);

  @override
  Future<Either<FailureEntity, List<ShortMovieEntity>>> call(
      SearchParams params) async {
    return await productRepository.searchProducts(params);
  }
}

class SearchParams {
  final String? query;
  final int? page;
  final int? limit;

  const SearchParams({
    this.query = '',
    this.page = 1,
    this.limit = 30,
  });
}
