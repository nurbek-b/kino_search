// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'actor_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$ActorModelImpl _$$ActorModelImplFromJson(Map<String, dynamic> json) =>
    _$ActorModelImpl(
      id: json['id'] as int,
      name: json['name'] as String,
      surname: json['surname'] as String,
      image: json['image'] as String,
    );

Map<String, dynamic> _$$ActorModelImplToJson(_$ActorModelImpl instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'surname': instance.surname,
      'image': instance.image,
    };
