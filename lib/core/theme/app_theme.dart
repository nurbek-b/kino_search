import 'package:flutter/material.dart';
import 'package:fox_video/core/theme/app_colors.dart';
import 'package:fox_video/core/theme/text_theme.dart';

class AppTheme extends ChangeNotifier {
  static const defaultScreenWidth = 414.0;
  static const defaultScreenHeight = 896.0;

  var screenScaleFactor = 1.0;

  void setScreenScaleFactor(double? currentScreenWidth) {
    screenScaleFactor = currentScreenWidth != null
        ? currentScreenWidth / defaultScreenWidth
        : screenScaleFactor;

    notifyListeners();
  }

  double get inputBorderWidth => scaledSize(1.0);

  double scaledSize(double size) => size * screenScaleFactor;

  Radius scaledRadius(double radius) =>
      Radius.circular(radius * screenScaleFactor);

  /// Light theme data of the app
  ThemeData get themeData => ThemeData(
    brightness: Brightness.light,
    primaryColor: AppColors.primary,
    textTheme: TextThemes.textTheme,
    primaryTextTheme: TextThemes.primaryTextTheme,
    colorScheme: const ColorScheme.light(
      primary: AppColors.primary,
      secondary: AppColors.lightGrey,
      error: AppColors.error,
    ),
    appBarTheme: const AppBarTheme(
      elevation: 0,
      backgroundColor: AppColors.primary,
    ),
  );
}
