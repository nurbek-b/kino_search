import 'package:dartz/dartz.dart';
import 'package:fox_video/core/error/error.dart';
import 'package:fox_video/core/use_case/base_use_case.dart';
import 'package:fox_video/features/movies/domain/repositories/movie_base_repository.dart';

class RemoveMovieUseCase extends BaseUseCase<void, RemoveMoviesParams> {
  final MovieBaseRepository _repository;

  RemoveMovieUseCase(this._repository);

  @override
  Future<Either<FailureEntity, void>> call(RemoveMoviesParams params) async {
    return await _repository.removeMovie(params);
  }
}

class RemoveMoviesParams {
  final String movieId;

  RemoveMoviesParams(this.movieId);
}
