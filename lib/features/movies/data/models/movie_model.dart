
import 'package:fox_video/features/movies/data/models/actor_model.dart';
import 'package:fox_video/features/movies/data/models/rating_model.dart';
import 'package:fox_video/features/movies/data/models/vote_model.dart';
import 'package:fox_video/features/movies/domain/entities/fee_entity.dart';
import 'package:fox_video/features/movies/domain/entities/genre_entity.dart';
import 'package:fox_video/features/movies/domain/entities/movie_entity.dart';

class MovieModel extends MovieEntity {
  MovieModel({
    required int id,
    required String name,
    required String description,
    required DateTime year,
    required RatingModel rating,
    required VoteModel votes,
    required List<Genre> genres,
    required DateTime? premiere,
    required String? teaser,
    required WorldFee? worldTotalFee,
    required List<ActorModel> actors,
  }) : super(
          id: id,
          name: name,
          description: description,
          year: year,
          rating: rating,
          votes: votes,
          genres: genres,
          premiere: premiere,
          teaser: teaser,
          worldTotalFee: worldTotalFee,
          actors: actors,
        );

  factory MovieModel.fromJson(Map<String, dynamic> json) {
    final actors = (json["persons"]
            .where((itemJson) => itemJson["profession"] == "актеры"))
        .map((actor) => ActorModel.fromJson(actor));
    final premiere = json["premiere"]["world"];
    final teasers = json["videos"]["teasers"];
    final worldTotalFee = json["fees"]["world"];
    return MovieModel(
      id: json["id"],
      name: json["name"],
      description: json["description"],
      year: json["year"],
      rating: json["rating"],
      votes: json["votes"],
      genres: json["genres"].map((genreJson) => Genre.fromJson(genreJson)),
      premiere: premiere != null ? DateTime.parse(premiere) : null,
      teaser: teasers != null ? teasers.first["url"] : null,
      worldTotalFee:
          worldTotalFee["value"] != null && worldTotalFee["currency"] != null
              ? WorldFee.fromJson(worldTotalFee)
              : null,
      actors: actors,
    );
  }

  factory MovieModel.fromFB(Map<String, dynamic> json) {
    final actors = json["persons"].map((actor) => ActorModel.fromJson(actor));
    final premiere = json["premiere"];
    return MovieModel(
      id: json["id"],
      name: json["name"],
      description: json["description"],
      year: json["year"],
      rating: json["rating"],
      votes: json["votes"],
      genres: json["genres"].map((genreJson) => Genre.fromJson(genreJson)),
      premiere: premiere != null ? DateTime.parse(premiere) : null,
      teaser: json["teasers"],
      worldTotalFee: json["world_total_fee"],
      actors: actors,
    );
  }

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "description": description,
        "year": year.toIso8601String(),
        "genres": genres.map((e) => e.toJson).toList(),
        "votes": VoteModel.fromEntity(votes).toJson(),
        "rating": RatingModel.fromEntity(rating).toJson(),
        "premiere": premiere,
        "teaser": teaser,
        "world_total_fee": worldTotalFee,
        "actors": actors
            .map((actor) => ActorModel.fromEntity(actor).toJson())
            .toList()
      };

  factory MovieModel.fromEntity(MovieEntity entity) => MovieModel(
      id: entity.id,
      name: entity.name,
      description: entity.description,
      year: entity.year,
      genres: entity.genres,
      votes: VoteModel.fromEntity(entity.votes),
      rating: RatingModel.fromEntity(entity.rating),
      premiere: entity.premiere,
      teaser: entity.teaser,
      worldTotalFee: entity.worldTotalFee,
      actors:
          entity.actors.map((actor) => ActorModel.fromEntity(actor)).toList());
}
