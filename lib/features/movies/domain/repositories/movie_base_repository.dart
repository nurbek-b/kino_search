import 'package:dartz/dartz.dart';
import 'package:fox_video/core/error/error.dart';
import 'package:fox_video/core/use_case/base_use_case.dart';
import 'package:fox_video/features/movies/domain/entities/movie_entity.dart';
import 'package:fox_video/features/movies/domain/entities/short_movie_entity.dart';
import 'package:fox_video/features/movies/domain/use_cases/fetch_movie_use_case.dart';
import 'package:fox_video/features/movies/domain/use_cases/fetch_movies_use_case.dart';
import 'package:fox_video/features/movies/domain/use_cases/remove_movie_use_case.dart';
import 'package:fox_video/features/movies/domain/use_cases/search_movie_use_case.dart';

import '../use_cases/save_movie_use_case.dart';

abstract class MovieBaseRepository {
  Future<Either<FailureEntity, List<ShortMovieEntity>>> fetchMovies(
      FetchMoviesParams params);

  Future<Either<FailureEntity, List<MovieEntity>>> fetchSelectedMovies(
      NoParams params);

  Future<Either<FailureEntity, MovieEntity>> fetchMovie(
      FetchMovieParams params);

  Future<Either<FailureEntity, void>> saveMovie(SaveMoviesParams params);

  Future<Either<FailureEntity, void>> removeMovie(RemoveMoviesParams params);

  Future<Either<FailureEntity, List<ShortMovieEntity>>> searchProducts(
      SearchParams params);
}
