class VoteEntity {
  final int kp;
  final int imdb;
  final int? tmdb;
  final int filmCritics;
  final int russianFilmCritics;

  const VoteEntity({
    required this.kp,
    required this.imdb,
    required this.filmCritics,
    required this.russianFilmCritics,
    this.tmdb,
  });
}
