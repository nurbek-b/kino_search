import 'package:flutter/material.dart';
import 'package:fox_video/core/extensions/context_extension.dart';
import 'package:fox_video/features/movies/domain/entities/short_movie_entity.dart';
import 'package:fox_video/features/movies/presentation/pages/movie_detail_page.dart';

class CustomListTile extends StatelessWidget {
  const CustomListTile({
    Key? key,
    required this.movie,
  }) : super(key: key);

  final ShortMovieEntity movie;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        InkWell(
          onTap: () {
            context.pushNamed(MovieDetail.routeName);
          },
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: ListTile(
              title: Text(movie.name),
              subtitle: Text(movie.shortDescription),
              trailing: Text(movie.rating.toString()),
            ),
          ),
        ),
        const Divider()
      ],
    );
  }
}
