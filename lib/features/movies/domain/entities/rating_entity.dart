class RatingEntity {
  final int kp;
  final int imdb;
  final int filmCritics;
  final int russianFilmCritics;

  const RatingEntity({
    required this.kp,
    required this.imdb,
    required this.filmCritics,
    required this.russianFilmCritics,
  });
}
