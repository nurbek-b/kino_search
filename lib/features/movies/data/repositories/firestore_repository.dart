import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:fox_video/core/error/error.dart';
import 'package:fox_video/core/repositories/data_repository.dart';
import 'package:fox_video/features/movies/data/models/movie_model.dart';
import 'package:fox_video/features/movies/domain/entities/movie_entity.dart';

class FireStoreRepository with WidgetsBindingObserver {
  static const _movieTable = 'movie_data';
  static const _timeLimit = 10;

  final DataRepository<MovieEntity> localMovieStorage;

  FireStoreRepository(this.localMovieStorage) {
    WidgetsBinding.instance.addObserver(this);
    _initFireStore();
  }

  late CollectionReference<MovieEntity>? movieDB;

  @override
  Future<void> didChangeAppLifecycleState(AppLifecycleState state) async {
    switch (state) {
      case AppLifecycleState.detached:
        await close();
        break;
      case AppLifecycleState.inactive:
      case AppLifecycleState.paused:
      case AppLifecycleState.hidden:
      case AppLifecycleState.resumed:
        break;
    }
  }

  Future<void> saveMovie(MovieEntity movie) async {
    try {
      await movieDB?.doc(movie.id.toString()).set(movie);
    } catch (_) {
      throw FireStoreException();
    }
  }

  Future<void> removeMovie(String id) async {
    try {
      await movieDB
          ?.doc(id)
          .delete()
          .timeout(const Duration(seconds: _timeLimit));
    } catch (_) {
      throw FireStoreException();
    }
  }

  Future<List<MovieEntity>> fetchSelectedMovies() async {
    try {
      final movies =
          (await movieDB?.get())?.docs.map((doc) => doc.data()).toList();
      return movies ?? <MovieEntity>[];
    } catch (_) {
      throw FireStoreException();
    }
  }

  Future<void> _initFireStore() async {
    final fb = FirebaseFirestore.instance
      ..settings = const Settings(
        persistenceEnabled: true,
        cacheSizeBytes: Settings.CACHE_SIZE_UNLIMITED,
      );

    try {
      movieDB = fb.collection(_movieTable).withConverter<MovieModel>(
            fromFirestore: (doc, _) => MovieModel.fromJson(doc.data()!),
            toFirestore: (movie, _) => movie.toJson(),
          );
    } catch (_) {
      throw FireStoreException();
    }
  }

  Future close() async => WidgetsBinding.instance.removeObserver(this);
}
