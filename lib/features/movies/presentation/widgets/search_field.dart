import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fox_video/core/extensions/context_extension.dart';
import 'package:fox_video/core/theme/app_colors.dart';
import 'package:fox_video/core/theme/text_styles.dart';
import 'package:fox_video/features/movies/presentation/manager/manager.dart';

class SearchWidget extends StatefulWidget {
  final bool showSuffix;

  const SearchWidget({super.key, this.showSuffix = false});

  @override
  State<SearchWidget> createState() => _SearchWidgetState();
}

class _SearchWidgetState extends State<SearchWidget> {
  static const duration = 400;
  final controller = TextEditingController();
  final textFocusNode = FocusNode();
  String? lastInputValue;

  @override
  Widget build(BuildContext context) {
    final appTheme = context.appTheme;
    return AnimatedContainer(
      width: appTheme.scaledSize(300),
      height: appTheme.scaledSize(40),
      duration: const Duration(milliseconds: duration),
      child: TextField(
        focusNode: textFocusNode,
        cursorColor: AppColors.primary,
        controller: controller,
        style: AppTextStyles.bodySm,
        onChanged: (inputValue) {
          if (lastInputValue != inputValue) {
            lastInputValue = inputValue;
            context.read<SearchBloc>().add(
                  TextChanged(query: inputValue),
                );
          }
        },
        decoration: InputDecoration(
          hintText: "Search...",
          hintStyle: AppTextStyles.bodySm,
          contentPadding: EdgeInsets.symmetric(
            vertical: appTheme.scaledSize(8),
          ),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20),
            borderSide: const BorderSide(
              color: AppColors.white,
            ),
          ),
          prefixIcon: Padding(
            padding: EdgeInsets.only(
              top: appTheme.scaledSize(12),
              bottom: appTheme.scaledSize(12),
            ),
            child: const Icon(Icons.search),
          ),
          suffix: widget.showSuffix
              ? FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Container(
                    alignment: Alignment.bottomRight,
                    padding: EdgeInsets.only(
                      right: appTheme.scaledSize(10),
                      top: appTheme.scaledSize(34),
                    ),
                    child: GestureDetector(
                      onTap: () {
                        setState(() {
                          lastInputValue = '';
                          controller.clear();
                        });
                      },
                      child: const Icon(Icons.clear),
                    ),
                  ),
                )
              : const SizedBox.shrink(),
        ),
      ),
    );
  }
}
