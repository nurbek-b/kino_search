import 'package:fox_video/features/movies/data/models/rating_model.dart';
import 'package:fox_video/features/movies/domain/entities/short_movie_entity.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'short_movie_model.freezed.dart';

part 'short_movie_model.g.dart';

@freezed
class ShortMovieModel extends ShortMovieEntity with _$ShortMovieModel {
  factory ShortMovieModel({
    required int id,
    required String name,
    required String shortDescription,
    required RatingModel rating,
  }) = _ShortMovieModel;

  factory ShortMovieModel.fromJson(Map<String, dynamic> json) =>
      _$ShortMovieModelFromJson(json);
}
