import 'package:fox_video/core/extensions/sql.dart';
import 'package:fox_video/core/network/network.dart';
import 'package:fox_video/core/repositories/data_repository.dart';
import 'package:fox_video/features/movies/data/data_sources/local/movie_local_data_source.dart';
import 'package:fox_video/features/movies/data/data_sources/remote/movie_remote_data_source.dart';
import 'package:fox_video/features/movies/data/models/actor_model.dart';
import 'package:fox_video/features/movies/data/models/movie_model.dart';
import 'package:fox_video/features/movies/data/repositories/firestore_repository.dart';
import 'package:fox_video/features/movies/data/repositories/moview_repository_impl.dart';
import 'package:fox_video/features/movies/domain/entities/movie_entity.dart';
import 'package:fox_video/features/movies/domain/repositories/movie_base_repository.dart';
import 'package:fox_video/features/movies/domain/use_cases/fetch_movie_use_case.dart';
import 'package:fox_video/features/movies/domain/use_cases/fetch_movies_use_case.dart';
import 'package:fox_video/features/movies/domain/use_cases/fetch_selected_movies_use_case.dart';
import 'package:fox_video/features/movies/domain/use_cases/remove_movie_use_case.dart';
import 'package:fox_video/features/movies/domain/use_cases/save_movie_use_case.dart';
import 'package:fox_video/features/movies/domain/use_cases/search_movie_use_case.dart';
import 'package:get_it/get_it.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

final GetIt sl = GetIt.instance;

class AppDependencies {
  Future<void> initialize() async {
    sl

      /// SQL
      ..registerSingleton(
          SQLInterface<MovieModel>(tableName: SQLInterface.movieDataTable))
      ..registerSingleton(
          SQLInterface<ActorModel>(tableName: SQLInterface.actorDataTable))
      ..registerLazySingleton<DataRepository<MovieEntity>>(
          () => MovieLocalDataSource(
                dataBase: sl(),
                dataConverter: (item) => MovieModel.fromJson(item),
              ))

      /// Repository
      ..registerLazySingleton<MovieBaseRepository>(
          () => MovieRepositoryImpl(sl(), sl(), sl()))

      /// Remote data source
      ..registerLazySingleton<MovieRemoteDataSource>(
          () => MovieRemoteDataSourceImpl(sl(), sl()))
      ..registerLazySingleton<FireStoreRepository>(
          () => FireStoreRepository(sl()))

      /// Use cases
      ..registerLazySingleton<FetchMoviesUseCase>(
          () => FetchMoviesUseCase(sl()))
      ..registerLazySingleton<RemoveMovieUseCase>(
          () => RemoveMovieUseCase(sl()))
      ..registerLazySingleton<SaveMovieUseCase>(() => SaveMovieUseCase(sl()))
      ..registerLazySingleton<SearchMoviesUseCase>(
          () => SearchMoviesUseCase(sl()))
      ..registerLazySingleton<FetchMovieUseCase>(() => FetchMovieUseCase(sl()))
      ..registerLazySingleton<FetchSelectedMoviesUseCase>(
          () => FetchSelectedMoviesUseCase(sl()))

      /// Core
      ..registerLazySingleton<NetworkInfo>(() => NetworkInfoImpl(sl()))
      ..registerLazySingleton<APIProvider>(() => APIProviderImpl())

      /// External
      ..registerLazySingleton(() => InternetConnectionChecker());
  }
}
