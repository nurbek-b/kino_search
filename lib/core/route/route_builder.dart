import 'package:flutter/material.dart';
import 'package:fox_video/core/extensions/context_extension.dart';
import 'package:fox_video/core/theme/app_theme.dart';
import 'package:fox_video/core/utilities/page_route_transition_builder.dart';
import 'package:fox_video/features/movies/presentation/pages/selected_movies_page.dart';
import 'package:fox_video/features/movies/presentation/pages/movie_detail_page.dart';
import 'package:fox_video/features/movies/presentation/pages/movie_list_page.dart';
import 'package:provider/provider.dart';

mixin RouteBuilder {
  // ignore: body_might_complete_normally_nullable
  Route? onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case MovieListPage.routeName:
      case null:
        return AppPageRoute(
            routeSetting: settings, screen: const MovieListPage());
      case SelectedMoviesPage.routeName:
        return AppPageRoute(
            routeSetting: settings, screen: const SelectedMoviesPage());
      case MovieDetail.routeName:
        return MovieDetail.route(movieId: settings.arguments as int);
    }
    return null;
  }

  Future<void> selectRoute(GlobalKey<NavigatorState> navigatorKey) async {
    navigatorKey.currentState!.context.read<AppTheme>().setScreenScaleFactor(
          navigatorKey.currentState!.context.screenWidth,
        );

    void startScreen() => navigatorKey.currentState!.context
        .pushAndReplaceNamed(MovieListPage.routeName);

    startScreen();
  }
}
