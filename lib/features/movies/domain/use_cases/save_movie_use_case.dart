import 'package:dartz/dartz.dart';
import 'package:fox_video/core/error/error.dart';
import 'package:fox_video/core/use_case/base_use_case.dart';
import 'package:fox_video/features/movies/domain/entities/movie_entity.dart';
import 'package:fox_video/features/movies/domain/repositories/movie_base_repository.dart';

class SaveMovieUseCase extends BaseUseCase<void, SaveMoviesParams> {
  final MovieBaseRepository _repository;

  SaveMovieUseCase(this._repository);

  @override
  Future<Either<FailureEntity, void>> call(SaveMoviesParams params) async {
    return await _repository.saveMovie(params);
  }
}

class SaveMoviesParams {
  final MovieEntity movie;

  SaveMoviesParams(this.movie);
}
