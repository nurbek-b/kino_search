part of 'search_bloc.dart';

abstract class SearchState extends Equatable {
  const SearchState();

  @override
  List<Object> get props => [];
}

class SearchInitial extends SearchState {}

class SearchStateEmpty extends SearchState {}

class SearchStateLoading extends SearchState {}

class SearchStateSuccess extends SearchState {
  final List<ShortMovieEntity> movies;
  final String query;

  const SearchStateSuccess({
    this.movies = const [],
    this.query = "",
  });

  @override
  List<Object> get props => [movies, query];
}

class SearchStateError extends SearchState {
  final ErrorObject errorObject;

  const SearchStateError({required this.errorObject});
}
