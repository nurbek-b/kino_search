part of 'movie_bloc.dart';

abstract class MovieEvent {}

class FetchMovies extends MovieEvent {}

class FetchMovie extends MovieEvent {
  final int movieId;

  FetchMovie(this.movieId);
}
